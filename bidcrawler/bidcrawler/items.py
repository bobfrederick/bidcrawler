# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class BidcrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    opp_source = scrapy.Field()
    opp_name = scrapy.Field()
    opp_details = scrapy.Field()
    opp_value = scrapy.Field()
    pass
