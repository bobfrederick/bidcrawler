from datetime import datetime
import scrapy
import pandas as pd
import numpy as np


class UclaSpider(scrapy.Spider):
    """ spider for crawling the UCLA public bids website """
    name = 'ucla'
    allowed_domains = ['capitalprograms.ucla.edu']
    start_urls = ['http://www.capitalprograms.ucla.edu/Contracts/Bidding/']

    def start_requests(self):
        """ return the requests to crawl """
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        df = pd.DataFrame()

        # clean out </br> tags from response first
        response = response.replace(body=response.body.replace(b'<br>', b''))
        names = response.xpath('//table/tr/td[2]/b/a/text()').getall()
        descriptions =  response.xpath("//table/tr/td[1]/h5[contains(text(),'Project Description')]/../../td[2]/text()").getall()
        descriptions_parsed = []
        # remove blank rows from the table
        for item in descriptions:
            if len(item.strip()) > 0:
                descriptions_parsed.append(item.strip())
        values = response.xpath("//table/tr/td[1]/h5[contains(text(),'Estimated')]/../../td[2]/text()").getall()
        values = [text.strip() for text in values]
        dates = [datetime.now().strftime('%Y-%m-%d %H:%M %p') for name in names]
        sources = ["UCLA Capital Programs" for name in names]

        df['name'] = pd.Series(names)
        df['description'] = pd.Series(descriptions_parsed)
        df['value'] = pd.Series(values)
        df['source'] = pd.Series(sources)
        df['date'] = pd.Series(dates)
        

        data = df.to_json('./ucla.json',orient='records')

        return data