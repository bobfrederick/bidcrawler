import numpy as np
import pandas as pd
from pprint import pprint

df = pd.DataFrame()

names = ["name1", "name2", "name3", "name4"]
descriptions = ["description_1", "description_2"]
values = ["value1", "value2", "value3", "value4"]


df['name'] = pd.Series(names)
df['description'] = pd.Series(descriptions)
df['value'] = pd.Series(values)

data = df.transpose
pprint(data)
data = df.to_json(orient="records")

pprint(data)
