# BidCrawler #

This README documents the project and getting started.

### BidCrawler is a collection of Scrapy Spiders built to find AEC projects on public bid websites ###

* The **scrapy** library was chosen because of its full-fledged capabilities and support. It's currently very active on github.
* To get this project up and going quickly we'll also use **scrapyrt** to let us create an API that returns JSON that way we won't need a database. We might change this in the future if requests are too high.
* [The idea of using scrapy to make an API comes from this article](https://crazydevblog.com/posts/scrapy-and-scrapyrt--how-to-create-your-own-api-from--almost--any-website)

### How do I get set up? ###

* $ pip install -r requirements
* Configuration TBD
* Dependencies are scrapy, scrapyrt
* Database configuration (FUTURE)
* Uses pytest, pytest-cov for testing
* Deployment instructions TBD

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* bfrederick@rios.com